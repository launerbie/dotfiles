require('nvim-tree').setup{}
require("bufferline").setup{}
require("telescope").load_extension'hoogle'

vim.cmd [[ colorscheme catppuccin-macchiato ]]

local opt = vim.opt
local g = vim.g

local function keymap(mode, combo, mapping, opts)
  local options = {noremap = true}
  if opts then
    options = vim.tbl_extend('force', options, opts)
  end
  vim.api.nvim_set_keymap(mode, combo, mapping, options)
end

g.mapleader = ','
--
--Hitting Enter clears to search highlights
keymap('n', '<CR>', ':noh<CR><CR>', {noremap = true})
keymap('n', '<C-f>', ':Telescope find_files theme=dropdown<CR>')
keymap('n', '<C-g>', ':Telescope live_grep  theme=dropdown<CR>')
keymap('n', '<leader>f', ':Telescope find_files theme=ivy<CR>')
keymap('n', '<leader>g', ':Telescope live_grep  theme=ivy<CR>')
keymap('n', '<leader>h', ':Telescope hoogle<CR>')
keymap('n', '{', ':bprevious<CR>')
keymap('n', '}', ':bnext<CR>')
keymap('n', '<s-x>', ':bdelete<CR>')
keymap('n', '<C-x>', ':bdelete<CR>')
keymap('n', '<C-p>', ':NvimTreeToggle<CR>')

-- Moving between splits
keymap('n', '<C-h>', '<C-w>h')
keymap('n', '<C-j>', '<C-w>j')
keymap('n', '<C-k>', '<C-w>k')
keymap('n', '<C-l>', '<C-w>l')

--g['airline#extensions#tabline#enabled'] = 1
--g['airline#extensions#tabline#buffer_nr_show'] = 1

opt.tabstop = 4 --The visual width of a \t
opt.shiftwidth = 2
opt.termguicolors = true
opt.clipboard = "unnamedplus"
opt.number = true
opt.autoindent = false
-- Always expand tabs into spaces
opt.expandtab = true
